using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public SOEnemy SOEnemy;
    private int vida;
    public int da�o;
    private float velocidad;
    public GameObject TorreObjetivo;
    public GameEventTransf eventoScoreCoin;

    // Start is called before the first frame update
    void Start()
    {
        this.vida = SOEnemy.vida;
        this.da�o = SOEnemy.da�o;
        this.velocidad = SOEnemy.velocidad;
        this.gameObject.GetComponent<SpriteRenderer>().sprite = SOEnemy.spriteEnemigo;
        Kamikaze();
    }

    

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Kamikaze() { 
        Vector2 direccion = TorreObjetivo.transform.position - this.transform.position;
        direccion.Normalize();
        this.transform.up = direccion;
        this.GetComponent<Rigidbody2D>().velocity = direccion * this.velocidad;
    }


    public void Da�ar()
    {
        if (vida > 0)
        {
            vida--;
            this.gameObject.GetComponent<AudioSource>().Play();
            Kamikaze();
        }
        if (vida <= 0)
        {            
            if (GManager.GMInstance.Coins < 9999 && !GManager.GMInstance.Torremuerta1) {
                GManager.GMInstance.Coins += SOEnemy.coinsOnKill;
                if (GManager.GMInstance.Coins >= 9999) GManager.GMInstance.Coins = 9999;
            }
            if (GManager.GMInstance.Points < 999999999 && !GManager.GMInstance.Torremuerta1) {
                GManager.GMInstance.Points += SOEnemy.pointsOnKill;
                if (GManager.GMInstance.Points >= 999999999) GManager.GMInstance.Points = 999999999;
            }            
            eventoScoreCoin.Raise(this.transform);
            gameObject.SetActive(false);
        }
    }

    public IEnumerator ImStillStanding()
    {
        yield return new WaitForSeconds(0.5f);
        Kamikaze();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer == 9) StartCoroutine(ImStillStanding());
    }

    public void ActualizarSO() {
        this.vida = SOEnemy.vida;
        this.da�o = SOEnemy.da�o;
        this.velocidad = SOEnemy.velocidad;
        this.gameObject.GetComponent<SpriteRenderer>().sprite = SOEnemy.spriteEnemigo;
        this.gameObject.GetComponent<Animator>().runtimeAnimatorController = SOEnemy.animacionC;
        this.gameObject.GetComponent<AudioSource>().clip = SOEnemy.audioOnDeath;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICoinsScoreController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ActualizarScore()
    {
        this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "SCORE: " + GManager.GMInstance.Points;
    }

    public void ActualizarCoins()
    {
        this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "X " + GManager.GMInstance.Coins;
    }

    public void ActualizarLvlCost() {
        this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text =""+GManager.GMInstance.CostLvlUp;
    }
}

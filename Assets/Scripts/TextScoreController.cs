using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextScoreController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (this.gameObject.name == "YScore") {
            this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "YOUR SCORE: " + GManager.GMInstance.Points;
        }
        else if (this.gameObject.name == "HScore") this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "HIGHSCORE: " + GManager.GMInstance.Highscore;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

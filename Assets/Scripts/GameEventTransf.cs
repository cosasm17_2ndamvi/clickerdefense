using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameEventTransf : ScriptableObject
{
    public List<GameEventListenerTransf> events = new List<GameEventListenerTransf>();
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>

    public void Raise(Transform tf)
    {
        for (int i = events.Count - 1; i >= 0; i--)
        {
            events[i].OnEventRaised(tf);
        }
    }

    public void RegisterListener(GameEventListenerTransf listener)
    {
        if (!events.Contains(listener))
            events.Add(listener);
    }

    public void UnregisterListener(GameEventListenerTransf listener)
    {
        if (events.Contains(listener))
            events.Remove(listener);
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalasController : MonoBehaviour
{
    // Start is called before the first frame update
    public float balaVel;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 7)
        {
            collision.gameObject.GetComponent<EnemyController>().Da�ar();
            this.gameObject.SetActive(false);
        }        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        this.gameObject.SetActive(false);
    }
}

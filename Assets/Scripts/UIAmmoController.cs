using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIAmmoController : MonoBehaviour
{
    private float Nammo;
    [SerializeField]
    SOPlayer SOTorre;
    // Start is called before the first frame update
    void Start()
    {
        Nammo = SOTorre.ammo;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "X"+Nammo.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void actualizarAmmo(Transform tf) {
        Nammo =  SOTorre.ammo;
        if(Nammo <=0) this.GetComponent<TMPro.TextMeshProUGUI>().text = "X" + 0;
        else this.GetComponent<TMPro.TextMeshProUGUI>().text = "X" + Nammo.ToString();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonParticle : MonoBehaviour
{

    [SerializeField]
    ParticleSystem PS;
    [SerializeField]
    GameObject buttonManager;
   
    // Start is called before the first frame update
    void Start()
    {
        buttonManager.GetComponent<ButtonManager>().hpPlus += activar;
        buttonManager.GetComponent<ButtonManager>().lvlUp += activar;
    }

    private void OnDestroy()
    {
        buttonManager.GetComponent<ButtonManager>().hpPlus -= activar;
        buttonManager.GetComponent<ButtonManager>().lvlUp -= activar;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void activar() { 
        PS.Play();
    }
}

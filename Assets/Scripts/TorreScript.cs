using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.Windows;

public class TorreScript : MonoBehaviour
{
    public SOPlayer SOTorre;
    private float MAXhp;
    private float hp;
    private float ammo;
    public Transform DetecTransform; //transform de donde estar el area de deteccion
    public float DetecRango; //rango-radio del area de deteccion
    public LayerMask attackableLayer; // layer a la que pertenece el objetivo atacado
    private RaycastHit2D[] objetivos; //objetivos dentro de su area de deteccion
    private float CldwnDisp; //Cooldown entre disparos
    private bool disparando;
    [SerializeField]
    private InputActionAsset InputActionAsset;
    private InputActionAsset Input;
    private InputAction PointerPosition;
    [SerializeField]
    private LayerMask torreLayerMask; //layer de la misma torre para que se detecte asi misma en los clicks
    public GameEventTransf eventoHealthBar;
    public GameEventTransf eventoAmmo;
    [SerializeField]
    private ParticleSystem sistemaParticulas;
    [SerializeField]
    private GameObject buttonManager;

    private void Awake()
    {
        Input = Instantiate(InputActionAsset); //Instanciem el asset (pf)
        Input.FindActionMap("Default").FindAction("Click").performed += MoreAmmo; //subscribim la funci� MoreAmmo quan performem la acci� click del InputActionAsset
        PointerPosition = Input.FindActionMap("Default").FindAction("Posicion"); // en aquesta no subscribim perque la acci� consisteix en retornar un valor d'on es trobi el mouse o el touch
        Input.FindActionMap("Default").Enable(); //Activem el ActionMap
    }

    private void OnDestroy()
    {
        buttonManager.GetComponent<ButtonManager>().lvlUp -= Evolucionar;
        Input.FindActionMap("Default").FindAction("Click").performed -= MoreAmmo;
        Input.FindActionMap("Default").Disable();
    }
    // Start is called before the first frame update
    void Start()
    {
        buttonManager.GetComponent<ButtonManager>().lvlUp += Evolucionar;
        activarTorre();
    }

    // Update is called once per frame
    void Update()
    {
        if(!disparando && ammo>0) DetectandShot();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(DetecTransform.position, DetecRango);
    }

    private void DetectandShot() {
        objetivos = Physics2D.CircleCastAll(DetecTransform.position, DetecRango, transform.right, 0f, attackableLayer);
        for (int i = 0; i < objetivos.Length; i++)
        {
            if (objetivos[i].collider.gameObject.layer == 7 && ammo>0) {
                disparando=true;
                Vector2 direObj = objetivos[i].transform.position - this.transform.position;
                direObj.Normalize();
                GameObject proyectil = BalasPoolController.Instance.Pium();
                proyectil.transform.position = this.transform.position;
                proyectil.GetComponent<Rigidbody2D>().velocity = direObj * proyectil.GetComponent<BalasController>().balaVel;
                ammo--;
                SOTorre.ammo = ammo;
                eventoAmmo.Raise(this.transform);
                StartCoroutine("CooldownDisp");
            }
        }
    }

    public IEnumerator CooldownDisp()
    { 
        yield return new WaitForSeconds(CldwnDisp);
        disparando = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer == 7)
        {
            hp = SOTorre.currentHP;
            this.gameObject.GetComponent<AudioSource>().Play();
            this.hp-= collision.gameObject.GetComponent<EnemyController>().da�o;
            SOTorre.currentHP = this.hp;
            eventoHealthBar.Raise(this.transform);
            collision.gameObject.SetActive(false);
            if (hp <= 0)
            {
                GManager.GMInstance.Torremuerta1 = true;
                if (GManager.GMInstance.Points >= GManager.GMInstance.Highscore) GManager.GMInstance.Highscore = GManager.GMInstance.Points;
                Destroy(gameObject);
                GManager.GMInstance.StopAudio();
                GManager.GMInstance.StartAudioGO();
                SceneManager.LoadScene("GameOverScene");
            }
        }
    }

    private void MoreAmmo(InputAction.CallbackContext context)
    {
        Vector2 pointerPosition = PointerPosition.ReadValue<Vector2>();
        //Raig que parteix de la cmera i va cap a l'escena
        Ray cameraRay = Camera.main.ScreenPointToRay(pointerPosition);
        //Efectuem el raycast i apliquem filtre de layers
        RaycastHit2D hit = Physics2D.Raycast(cameraRay.origin, cameraRay.direction, Mathf.Infinity, torreLayerMask);
        //En cas d'impactar la torre aumentem la munici� en 1      
        if (hit.rigidbody != null)
        {
            ammo++; 
            SOTorre.ammo = ammo;
            Vector3 v3 = Camera.main.ScreenToWorldPoint(pointerPosition);            
            sistemaParticulas.transform.position = v3;
            sistemaParticulas.Play();
            eventoAmmo.Raise(this.transform);
        }

    }

    private void Evolucionar() {
        if (SOTorre.cldwndisp > 0.1f)
        {
            SOTorre.cldwndisp -= 0.02f;
            CldwnDisp = SOTorre.cldwndisp;
            SOTorre.HPMAX += 30;
            MAXhp = SOTorre.HPMAX;
            SOTorre.currentHP += 30;
            hp = SOTorre.currentHP;
        }
        else { 
            buttonManager.GetComponent<ButtonManager>().enabled = false;
        }
    }

    public void activarTorre() {
        disparando = false;
        MAXhp = SOTorre.HPMAX;
        hp = SOTorre.currentHP;
        ammo = SOTorre.ammo;
        CldwnDisp = SOTorre.cldwndisp;
    }

}

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private SOEnemy[] listaSOEnemigos;
    public float SpawnCldwn;
    [SerializeField]
    private GameObject enemyPF;
    [SerializeField]
    private GameObject objetivoEnemigos;
    private int puntero = 1;
    void Start()
    {
        StartCoroutine("Spawnea");
        StartCoroutine("Oleaje");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator Spawnea()
    {
        while (true)
        {
            int randomito = Random.Range(0, puntero); //el tama�o del puntero cambia en la corrutina
            int posRand = Random.Range(0, 4);
            GameObject enemy = BalasPoolController.Instance.Summon();
            enemy.GetComponent<EnemyController>().TorreObjetivo = objetivoEnemigos;
            switch (posRand)
            {
                case 0:
                    enemy.GetComponent<EnemyController>().SOEnemy = listaSOEnemigos[randomito];
                    enemy.transform.position = new Vector3(Random.Range(-10f, 10f), 6, 0);
                    enemy.GetComponent<EnemyController>().ActualizarSO();
                    enemy.GetComponent<EnemyController>().Kamikaze();
                    break;
                case 1:
                    enemy.GetComponent<EnemyController>().SOEnemy = listaSOEnemigos[randomito];
                    enemy.transform.position = new Vector3(Random.Range(-10f, 10f), -6, 0);
                    enemy.GetComponent<EnemyController>().ActualizarSO();
                    enemy.GetComponent<EnemyController>().Kamikaze();
                    break;
                case 2:
                    enemy.GetComponent<EnemyController>().SOEnemy = listaSOEnemigos[randomito];
                    enemy.transform.position = new Vector3(-10, Random.Range(-6f, 6f), 0);
                    enemy.GetComponent<EnemyController>().ActualizarSO();
                    enemy.GetComponent<EnemyController>().Kamikaze();
                    break;
                case 3:
                    enemy.GetComponent<EnemyController>().SOEnemy = listaSOEnemigos[randomito];
                    enemy.transform.position = new Vector3(10, Random.Range(-6f, 6f), 0);
                    enemy.GetComponent<EnemyController>().ActualizarSO();
                    enemy.GetComponent<EnemyController>().Kamikaze();
                    break;
            }
            yield return new WaitForSeconds(SpawnCldwn);
        }
        
    }

    public IEnumerator Oleaje() {
        bool flag1 = false;
        bool flag2 = false;
        bool flag3 = false;
        bool flag4 = false;
        int punt = 0;
        int tam = 0;
        while (!flag4)
        {
            print("New Wave");
            if (flag1 && !flag2)
            {
                if (puntero != listaSOEnemigos.Length) puntero++;
                else flag2 = true;
            }
            else if (flag1 && flag2 && !flag3)
            {
                if(SpawnCldwn >0.4f) SpawnCldwn -= 0.1f;
                if (punt != listaSOEnemigos.Length - 1)
                {
                    listaSOEnemigos[punt].da�o *= 2;
                    listaSOEnemigos[punt].vida *= 2;
                    punt++;
                }
                else
                {
                    punt = 0;
                    flag3 = true;
                    tam = listaSOEnemigos.Length-1;
                }
            }
            else if(flag1 && flag2 && flag3) {
                if (SpawnCldwn > 0.4f) SpawnCldwn -= 0.1f;
                int rand = Random.Range(1, listaSOEnemigos.Length);
                if (punt != tam) {
                    listaSOEnemigos.Append(listaSOEnemigos[rand]);
                    punt++;
                }
                else flag4 = true;
            }
            if (!flag1) flag1 = true;
            yield return new WaitForSeconds(30);
        }
        
    }
}

using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{

    public delegate void Act();
    public event Act activarEscudo;
    public event Act hpPlus;
    public event Act lvlUp;
    public GameEventTransf eventoCoinsScore;
    public bool activado_escudo = false;
    // Start is called before the first frame update
    void Start()
    {
        eventoCoinsScore.Raise(this.transform);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Escudar() {
        if (GManager.GMInstance.Coins >= GManager.GMInstance.CostShield && !activado_escudo) {
            this.GetComponent<AudioSource>().Play();
            activado_escudo=true;
            GManager.GMInstance.Coins -=GManager.GMInstance.CostShield;
            eventoCoinsScore.Raise(this.transform);
            activarEscudo?.Invoke();
        }
    }

    public void PLusVida()
    {
        if (GManager.GMInstance.Coins >= GManager.GMInstance.CostHpPlus)
        {
            this.GetComponent<AudioSource>().Play();
            GManager.GMInstance.Coins -=GManager.GMInstance.CostHpPlus;
            eventoCoinsScore.Raise(this.transform);
            hpPlus?.Invoke();
        }
    }

    public void Levelear()
    {
        if (GManager.GMInstance.Coins >= GManager.GMInstance.CostLvlUp)
        {
            this.GetComponent<AudioSource>().Play();
            GManager.GMInstance.Coins -=GManager.GMInstance.CostLvlUp;
            GManager.GMInstance.CostLvlUp += GManager.GMInstance.CostLvlUp;
            eventoCoinsScore.Raise(this.transform);
            lvlUp?.Invoke();
        }
    }

    public void Exit()
    {
        this.GetComponent<AudioSource>().Play();
        GManager.GMInstance.Points = 0;
        GManager.GMInstance.Coins = 0;
        GManager.GMInstance.CostHpPlus = 50; //coste predefinido
        GManager.GMInstance.CostShield = 25; //coste predefinido
        GManager.GMInstance.CostLvlUp = 100; //coste predefinido
        GManager.GMInstance.ReseteoEnemy1();
        GManager.GMInstance.ReseteoEnemy2();
        GManager.GMInstance.ReseteoEnemy3();
        GManager.GMInstance.ReseteoTorre();
        UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    public void Retry() {
        this.GetComponent<AudioSource>().Play();
        GManager.GMInstance.Points = 0;
        GManager.GMInstance.Coins = 0;
        GManager.GMInstance.CostHpPlus = 50; //coste predefinido
        GManager.GMInstance.CostShield = 25; //coste predefinido
        GManager.GMInstance.CostLvlUp = 100; //coste predefinido
        GManager.GMInstance.ReseteoEnemy1();
        GManager.GMInstance.ReseteoEnemy2();
        GManager.GMInstance.ReseteoEnemy3();
        GManager.GMInstance.ReseteoTorre();
        GManager.GMInstance.StartAudioIngame();
        SceneManager.LoadScene("SampleScene");
    }

    public void Menu() {
        GManager.GMInstance.StartAudioInMenu();
        this.GetComponent<AudioSource>().Play();
        SceneManager.LoadScene("Inicio");
    }

    public void Credits() {
        this.GetComponent<AudioSource>().Play();
        SceneManager.LoadScene("Credits");
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverTextController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(this.gameObject.transform.position.x > 0) this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(-20, 0, 0);
        else this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(20, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }
}

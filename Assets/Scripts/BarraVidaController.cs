using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class BarraVidaController : MonoBehaviour
{
    
    public Gradient gradient;
    public Image fill;   
    private float maxHp;
    private float currentHp;
    private float lastHp;
    [SerializeField]
    private float lerpTime;
    [SerializeField]
    SOPlayer SOTorre;
    [SerializeField]
    private GameObject buttonManager;
    // Start is called before the first frame update

    void Start()
    {
        buttonManager.GetComponent<ButtonManager>().hpPlus += CambiaVida;
        maxHp = SOTorre.HPMAX;
        currentHp = SOTorre.currentHP;
        fill.fillAmount = currentHp/maxHp;
        fill.color = gradient.Evaluate(fill.fillAmount);
    }

    public void perderVida(Transform tf)
    {
        StopAllCoroutines();
        //StopCoroutine("ActualitzarVida");
        lastHp = currentHp;
        currentHp = SOTorre.currentHP;
        StartCoroutine(ActualitzarVida(currentHp > lastHp));
    }

    private void CambiaVida() {
        StopAllCoroutines();
        //StopCoroutine("ActualitzarVida");
        SOTorre.currentHP += 50;
        if (SOTorre.currentHP >= SOTorre.HPMAX) SOTorre.currentHP = SOTorre.HPMAX;
        lastHp = currentHp;
        currentHp = SOTorre.currentHP;
        StartCoroutine(ActualitzarVida(currentHp > lastHp));
    }

    private IEnumerator ActualitzarVida(bool incrementar)
    {
        float currentTime = 0f;
        maxHp = SOTorre.HPMAX;
        while (currentTime <= lerpTime)
        {
            if (!incrementar) fill.fillAmount = Mathf.Lerp(lastHp / maxHp, currentHp / maxHp, currentTime / lerpTime);
            else
            {
                fill.fillAmount =  Mathf.Lerp(lastHp / maxHp, currentHp / maxHp, currentTime / lerpTime);
            }
            fill.color = gradient.Evaluate(fill.fillAmount);
            yield return null;
            currentTime += Time.deltaTime;
        }
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EscudoController : MonoBehaviour
{
    public GameObject buttonManager;
    // Start is called before the first frame update
    void Start()
    {
       this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
       this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
    }

    private void Awake()
    {
        buttonManager.GetComponent<ButtonManager>().activarEscudo += Activacion;
    }

    private void OnDestroy()
    {
        buttonManager.GetComponent<ButtonManager>().activarEscudo -= Activacion;
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    private void Activacion() {
        StartCoroutine(ONandOff());
    }

    public IEnumerator ONandOff() {
        this.gameObject.GetComponent<CircleCollider2D>().enabled = true;
        this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        Color color = buttonManager.GetComponent<Image>().color;
        buttonManager.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0.8f);
        yield return new WaitForSeconds(3);
        this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
        this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        buttonManager.GetComponent<Image>().color = color;
        buttonManager.GetComponent<ButtonManager>().activado_escudo = false;
    }
}

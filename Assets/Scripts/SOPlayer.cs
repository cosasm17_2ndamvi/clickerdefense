using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SOPlayer : ScriptableObject
{
    public float HPMAX;
    public float currentHP;
    public float ammo;
    public float cldwndisp;
}

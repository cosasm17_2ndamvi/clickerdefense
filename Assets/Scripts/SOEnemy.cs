using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

[CreateAssetMenu]
public class SOEnemy : ScriptableObject
{
    public int vida;
    public int da�o;
    public float velocidad;
    public Sprite spriteEnemigo;
    public int pointsOnKill;
    public int coinsOnKill;
    public AudioClip audioOnDeath;
    public RuntimeAnimatorController animacionC;
}

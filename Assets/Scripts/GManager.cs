using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GManager : MonoBehaviour
{
    //Singleton
    private static GManager gmanager;
    public static GManager GMInstance { get { return gmanager; } }

    public int Points { get => points; set => points = value; }
    public int Coins { get => coins; set => coins = value; }
    public int CostHpPlus { get => costHpPlus; set => costHpPlus = value; }
    public int CostShield { get => costShield; set => costShield = value; }
    public int CostLvlUp { get => costLvlUp; set => costLvlUp = value; }
    public int Highscore { get => highscore; set => highscore = value; }
    public bool Torremuerta1 { get => Torremuerta; set => Torremuerta = value; }

    private int points;
    [SerializeField]
    private int coins;
    [SerializeField]
    private int costHpPlus;
    [SerializeField]
    private int costShield;
    [SerializeField]
    private int costLvlUp;
    //Scriptable Objects
    [SerializeField]
    private SOPlayer SOTorre;
    [SerializeField]
    private SOEnemy SOEnemy1;
    [SerializeField]
    private SOEnemy SOEnemy2;
    [SerializeField]
    private SOEnemy SOEnemy3;
    private int highscore;
    [SerializeField]
    private AudioClip audioInGame;
    [SerializeField]
    private AudioClip audioInMenu;
    [SerializeField]
    private AudioClip audioInGameOver;
    private bool Torremuerta;

    private void Awake()
    {
        if (gmanager == null) { gmanager = this; }
        else { Destroy(gameObject); }
        DontDestroyOnLoad(gameObject);
        Torremuerta = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
        StartAudioInMenu();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReseteoTorre() {
        Torremuerta = false;
        SOTorre.ammo = 0;
        SOTorre.cldwndisp = 0.4f;
        SOTorre.HPMAX = 300;
        SOTorre.currentHP = 300;
    }

    public void ReseteoEnemy1() {
        SOEnemy1.da�o = 15;
        SOEnemy1.vida = 1;
        SOEnemy1.velocidad = 5;
    }
    public void ReseteoEnemy2()
    {
        SOEnemy2.da�o = 25;
        SOEnemy2.vida = 2;
        SOEnemy2.velocidad = 4;
    }
    public void ReseteoEnemy3()
    {
        SOEnemy3.da�o = 40;
        SOEnemy3.vida = 3;
        SOEnemy3.velocidad = 3;
    }

    public void StartAudioIngame() {
        this.gameObject.GetComponent<AudioSource>().clip = audioInGame;
        this.gameObject.GetComponent<AudioSource>().Play();
    }

    public void StartAudioInMenu()
    {
        this.gameObject.GetComponent<AudioSource>().clip = audioInMenu;
        this.gameObject.GetComponent<AudioSource>().Play();
    }

    public void StartAudioGO()
    {
        this.gameObject.GetComponent<AudioSource>().clip = audioInGameOver;
        this.gameObject.GetComponent<AudioSource>().Play();
    }

    public void StopAudio() {
        this.gameObject.GetComponent<AudioSource>().Stop();
    }
}
